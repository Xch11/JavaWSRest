package org.ricardo.restws.test.complete.address;

import javax.net.ssl.SSLEngineResult.Status;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.ricardo.restws.test.complete.bd.DataBaseTest;

@Path("address")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class addressResource {
	
	@GET
	public Address getAddress(@MatrixParam("idAddress") int id)
	{
		return DataBaseTest.arrayAddress.get(id);
	}
	
	@POST
	public Address postAddress(@HeaderParam("numAddress") int num, 
								@HeaderParam("localAddress") String local, 
								@HeaderParam("country") String country)
	{
		
		DataBaseTest.arrayAddress.put(DataBaseTest.arrayAddress.size()+1, new Address(num,local,country));
		return DataBaseTest.arrayAddress.get(DataBaseTest.arrayAddress.size());
	}
	
	@PUT
	public Response updateAddress(@CookieParam("idAddress") int id, Address address)
	{
		Address addr = DataBaseTest.arrayAddress.get(id);
		addr.setNumAddress(address.getNumAddress());
		addr.setLocalAddress(address.getLocalAddress());
		addr.setCountry(address.getCountry());
		
		return Response.status(javax.ws.rs.core.Response.Status.OK)
				.entity("UPDATED!")
				.build();
	}
	
	@POST
	@Path("/insert")
	public Address postAddress2(@BeanParam AddressBean addr)
	{
		addr = new AddressBean(addr.getNumAddress(),addr.getLocalAddress(), addr.getCountry());
		return DataBaseTest.arrayAddress.get(DataBaseTest.arrayAddress.size());
	}

}
