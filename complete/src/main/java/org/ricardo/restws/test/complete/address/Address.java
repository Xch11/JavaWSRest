package org.ricardo.restws.test.complete.address;

import javax.ws.rs.QueryParam;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Address {
	
	private int numAddress;
	private String localAddress;
	private String country;
	
	public Address()
	{
		
	}
	
	public Address(int numAddress, String localAddress, String country) {
		this.numAddress = numAddress;
		this.localAddress = localAddress;
		this.country = country;
	}
	
	public int getNumAddress() {
		return numAddress;
	}
	public void setNumAddress(int numAddress) {
		this.numAddress = numAddress;
	}
	public String getLocalAddress() {
		return localAddress;
	}
	public void setLocalAddress(String localAddress) {
		this.localAddress = localAddress;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

}
