package org.ricardo.restws.test.complete.address;

import javax.ws.rs.QueryParam;

import org.ricardo.restws.test.complete.bd.DataBaseTest;

public class AddressBean {

	@QueryParam("numAddress") int numAddress;
	@QueryParam("localAddress") String localAddress;
	@QueryParam("country")String country;
	
	public AddressBean()
	{
		
	}
	
	public AddressBean(int numAddress, String localAddress, String country) {
		DataBaseTest.arrayAddress.put(DataBaseTest.arrayAddress.size()+1, new Address(numAddress,localAddress,country));
	}

	public int getNumAddress() {
		return numAddress;
	}
	public void setNumAddress(int numAddress) {
		this.numAddress = numAddress;
	}
	public String getLocalAddress() {
		return localAddress;
	}
	public void setLocalAddress(String localAddress) {
		this.localAddress = localAddress;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	
}
