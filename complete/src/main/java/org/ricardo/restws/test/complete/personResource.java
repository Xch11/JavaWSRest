package org.ricardo.restws.test.complete;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.ricardo.restws.test.complete.bd.DataBaseTest;

@Path("person")
@Produces(MediaType.APPLICATION_JSON)
//@Consumes(MediaType.APPLICATION_JSON)
public class personResource {

	List<Person> listPerson = new ArrayList<Person>();
	
	@GET
	@Path("getPerson")
	public List<Person> getPersons()
	{
		for(Person pers : DataBaseTest.arrayPerson.values())
		{
			listPerson.add(pers);
		}
		return listPerson;
	}
	
	@POST
	@Path("createPerson")
	public Person newPerson(Person pers)
	{
		DataBaseTest.arrayPerson.put(DataBaseTest.arrayPerson.size() + 1, pers);
		return DataBaseTest.arrayPerson.get(DataBaseTest.arrayPerson.size());
	}
	
	@PUT
	@Path("updatePerson")
	public Person newPerson(@QueryParam("personId") int id, Person pers)
	{
		Person pers2 = DataBaseTest.arrayPerson.get(id);
		pers2.setLastName(pers.getLastName());
		pers2.setMiddleName(pers.getMiddleName());
		pers2.setName(pers.getName());
		return DataBaseTest.arrayPerson.get(id);
	}
	
	@DELETE
	@Path("/deletePerson/{personId}")
	public String deletePerson(@PathParam("personId") int id)
	{
		DataBaseTest.arrayPerson.remove(id);
		return "Deleted!";
	}
	
}
